<?php
// \$Id\$

/**
 * @file
 * Code beautifier include.
 * Provides functions for code formatting.
 *
 */

/**
 * Implementation of hook_beautifier_functions().
 */
function beautifier_beautifier_functions() {
  // list of functions and their description implemented by default in
  // beautifier.inc.
  return array(
    'beautifier_line_breaks_win' => array(
      '#title' => t('Convert Windows line breaks to Unix format.'),
    ),
    'beautifier_line_breaks_mac' => array(
      '#title' => t('Convert Macintosh line breaks to Unix format.'),
    ),
    'beautifier_opening_php_full' => array(
      '#title' => t('Always use &lt;?php ?&gt; to delimit PHP code, not the &lt;? ?&gt; shorthands.'),
    ),
    'beautifier_switch_exit' => array(
      '#title' => t('Either exit a switch case with return *or* break.'),
    ),
    'beautifier_inline_comment' => array(
      '#title' => t('Move inline comments above remarked line.'),
    ),
    'beautifier_format_php' => array(
      '#title' => t('Format string according to PHP rules. (needs to be split into separate functions)'),
    ),
    'beautifier_cvs_id' => array(
      '#title' => t('If the CVS keyword Id already exists, append a new line after it.'),
    ),
    'beautifier_multiple_vars' => array(
      '#title' => t('Align equal signs of multiple variable assignments in the same column.'),
    ),
    'beautifier_curly_braces' => array(
      '#title' => t('Use curly braces even in situations where they are technically optional.'),
    ),
    'beautifier_trim' => array(
      '#title' => t('Remove surrounding whitespace.'),
    ),
    'beautifier_insert_cvs_id' => array(
      '#title' => t('Insert Drupal CVS keyword Id.'),
    ),
    'beautifier_remove_closing_php' => array(
      '#title' => t('Remove closing PHP tag.'),
    ),
    'beautifier_append_two_lines' => array(
      '#title' => t('Append two empty lines.'),
    ),
  );
}

/**
 * Implementation of hook_beautifier_presets().
 */
function beautifier_beautifier_presets() {
  return array(
    'php' => array(
      'drupal' => array(
        '#title' => t('Drupal coding standards'),
        '#url' => 'http://drupal.org/coding-standards',
        '#functions' => array(  // the order of these is significant
          'beautifier_line_breaks_win',
          'beautifier_line_breaks_mac',
          'beautifier_opening_php_full',
          'beautifier_switch_exit',
          'beautifier_inline_comment',
          'beautifier_format_php',
          'beautifier_cvs_id',
          'beautifier_multiple_vars',
          'beautifier_curly_braces',
          'beautifier_trim',
          'beautifier_insert_cvs_id',
          'beautifier_remove_closing_php',
          'beautifier_append_two_lines',
        ),
      ),
      /*
      'pear' => array(
        '#title' => t('PEAR coding standards'),
        '#url' => 'http://pear.php.net/manual/en/standards.php',
      ),
      'zend' => array(
        '#title' => t('Zend framework coding standards'),
        '#url' => 'http://framework.zend.com/manual/en/coding-standard.html',
      ),
      */
    ),
    /*
    'html' => array(

    ),
    'css' => array(

    ),
    */
  );

}

/**
 * Format the source code according to Drupal coding style guidelines.
 *
 * This function uses PHP's tokenizer functions.
 * @see <http://www.php.net/manual/en/ref.tokenizer.php>
 *
 * To achieve the desired coding style, we have to take some special cases
 * into account. These are:
 *
 * Indent-related:
 *   $_beautifier_indent int Indent level
 *      The number of indents for the next line. This is
 *      - increased after {, : (after case and default).
 *      - decreased after }, break, case and default (after a previous case).
 *   $in_case bool
 *      Is true after case and default. Is false after break and return, if
 *      $braces_in_case is not greater than 0.
 *   $switches int Switch level
 *      Nested switches need to have extra indents added to them.
 *   $braces_in_case array Count of braces
 *      The number of currently opened curly braces in a case. This is needed
 *      to support arbitrary function exits inside of a switch control strucure.
 *      This is an array to allow for nested switches.
 *   $parenthesis int Parenthesis level
 *      The number of currently opened parenthesis. This
 *      - prevents line feeds in brackets (f.e. in arguments of for()).
 *      - is the base for formatting of multiline arrays. Note: If the last
 *        ');' is not formatted to the correct indent level then there is no
 *        ',' (comma) behind the last array value.
 *   $in_brace bool
 *      Is true after left curly braces if they are in quotes, an object or
 *      after a dollar sign. Prevents line breaks around such variable
 *      statements.
 *   $in_heredoc bool
 *      Is true after heredoc output method and false after heredoc delimiter.
 *      Prevents line breaks in heredocs.
 *   $first_php_tag bool
 *      Is false after the first PHP tag. Allows inserting a line break after
 *      the first one.
 *   $in_do_while bool
 *      Is true after a do {} statement and set to false in the next while
 *      statement. Prevents a line break in the do {...} while() construct.
 *
 * Whitespace-related:
 *   $in_object bool
 *      Prevents whitespace after ->.
 *      Is true after ->. Is reset to false after the next string or variable.
 *   $in_at bool
 *      Prevents whitespace after @.
 *      Is true after @. Is reset to false after the next string or variable.
 *   $in_quote bool
 *      Prevents
 *      - removal of whitespace in double quotes.
 *      - injection of new line feeds after brackets in double quotes.
 *   $inline_if bool
 *      Controls formatting of ? and : for inline ifs until a ; (semicolon) is
 *      processed.
 *   $in_function_declaration
 *      Prevents whitespace after & for function declarations, e.g.
 *      function &foo(). Is true after function token but before first
 *      parenthesis.
 *   $in_array
 *      Array of parenthesis level to whether or not the structure
 *      is for an array.
 *   $in_multiline
 *      Array of parenthesis level to whether or not the structure
 *      is multiline.
 *
 * Context flags:
 *   These variables give information about what tokens have just been
 *   processed so that operations can change their behavior depending on
 *   the preceding context without having to scan backwards on the fully
 *   formed result. Most of these are ad hoc and have a very specific
 *   purpose in the program. It would probably be a good idea to generalize
 *   this facility.
 *
 *   $after_semicolon
 *      Is the token being processed on the same line as a semicolon? This
 *      allows for the semicolon processor to unconditionally add a newline
 *      while allowing things like inline comments on the same line to
 *      be bubbled up.
 *   $after_case
 *      Is the token being processed on the same line as a case? This
 *      is a specific override for comment movement behavior that places
 *      inline comments after a case before the case declaration.
 *   $after_comment
 *      Is the line being processed preceded by an inline comment?
 *      This is used to preserve newlines after comments.
 *   $after_initial_comment
 *      Is the line being processed preceded by the // $Id
 *      (ending dollar sign omitted) comment? This is a workaround to
 *      prevent the usual double-newline before docblocks for the very
 *      first docblock.
 *   $after_visibility_modifier
 *      Is the token being processed immediately preceded by a
 *      visibility modifier like public/protected/private? This prevents
 *      extra newlines added by T_FUNCTION.
 *   $after_return_in_case
 *      Whether or not the token is after a return statement in a case.
 *      This prevents the extra indent after case statements from being
 *      terminated prematurely for multiline return lines.
 *
 * @param $code
 *      The source code to format.
 *
 * @return
 *      The formatted code or false if it fails.
 */
function beautifier_format_php($code = '') {
  global $_beautifier_indent;

  // Indent controls:
  $_beautifier_indent  = 0;
  $in_case        = false;
  $switches       = 0;
  $parenthesis    = 0;
  $braces_in_case = array();
  $in_brace       = false;
  $in_heredoc     = false;
  $first_php_tag  = true;
  $in_do_while    = false;

  // Whitespace controls:
  $in_object    = FALSE;
  $in_at        = FALSE;
  $in_php       = FALSE;
  $in_quote     = FALSE;
  $inline_if    = FALSE;
  $in_array     = array();
  $in_multiline = array();

  // Context flags:
  $after_semicolon = FALSE;
  $after_case = FALSE;
  $after_comment = FALSE;
  $after_initial_comment = FALSE;
  $after_visibility_modifier = FALSE;
  $after_return_in_case = FALSE;
  $after_php = FALSE;

  // Whether or not a function token was encountered:
  $in_function_declaration = FALSE;

  // The position of the last character of the last non-whitespace
  // non-comment token, e.g. it would be:
  // function foo() { // bar
  //                ^ this character
  $position_last_significant_token = 0;

  $result    = '';
  $lasttoken = array(0);
  $tokens    = token_get_all($code);

  // Mask T_ML_COMMENT (PHP4) as T_COMMENT (PHP5).
  if (!defined('T_ML_COMMENT')) {
    define('T_ML_COMMENT', T_COMMENT);
  }
  // Mask T_DOC_COMMENT (PHP5) as T_ML_COMMENT (PHP4).
  else if (!defined('T_DOC_COMMENT')) {
    define('T_DOC_COMMENT', T_ML_COMMENT);
  }

  foreach ($tokens as $token) {
    if (is_string($token)) {
      // Simple 1-character token.
      $text = trim($token);
      switch ($text) {
        case '{':
          // Add a space before and behind a curly brace, if we are in inline
          // PHP, e.g. <?php if ($foo) { print $foo }
          if ($after_php) {
            $text = " $text ";
          }
          // Write curly braces at the end of lines followed by a line break if
          // not in quotes (""), object ($foo->{$bar}) or in variables (${foo}).
          // (T_DOLLAR_OPEN_CURLY_BRACES exists but is never assigned.)
          $c = substr(rtrim($result), -1);
          if (!$after_php && !$in_quote && (!$in_variable && !$in_object && $c != '$' || $c == ')')) {
            if ($in_case) {
              ++$braces_in_case[$switches];
              $_beautifier_indent += $switches - 1;
            }
            ++$_beautifier_indent;
            $result = rtrim($result) .' '. $text;
            beautifier_br($result);
          }
          else {
            $in_brace = true;
            $result .= $text;
          }
          break;

        case '}':
          if (!$in_quote && !$in_brace && !$in_heredoc) {
            if ($switches) {
              --$braces_in_case[$switches];
            }
            --$_beautifier_indent;
            if ($braces_in_case[$switches] < 0 && $in_case) {
              // Decrease indent if last case in a switch is not terminated.
              --$_beautifier_indent;
              $in_case = FALSE;
            }
            if ($braces_in_case[$switches] < 0) {
              $braces_in_case[$switches] = 0;
              $switches--;
            }
            if ($switches > 0) {
              $in_case = TRUE;
            }

            if (!$after_php) {
              $result = rtrim($result);
              if (substr($result, -1) != '{') {
                // Avoid line break in empty curly braces.
                beautifier_br($result);
              }
              $result .= $text;
              beautifier_br($result);
            }
            else {
              // Add a space before a curly brace, if we are in inline PHP, e.g.
              // <?php if ($foo) { print $foo }
              $result = rtrim($result, ' ');
              if (substr($result, -1) !== "\n") {
                $result .= ' ';
              }
              $result .= $text;
            }
          }
          else {
            $in_brace = false;
            $result .= $text;
          }
          break;

        case ';':
          $result = rtrim($result) . $text;
          // Check if we had deferred reduction of indent because we were in
          // a case statement. Now we can decrease the indent.
          if ($after_return_in_case) {
            --$_beautifier_indent;
            $after_return_in_case = FALSE;
          }
          if (!$parenthesis && !$in_heredoc && !$after_php) {
            beautifier_br($result);
            $after_semicolon = TRUE;
          }
          else {
            $result .= ' ';
          }
          if ($inline_if) {
            $inline_if = false;
          }
          break;

        case '?':
          $inline_if = true;
          $result .= ' '. $text .' ';
          break;

        case ':':
          if ($inline_if) {
            $result .= ' '. $text .' ';
          }
          elseif ($after_php) {
            $result .= $text;
          }
          else {
            if ($in_case) {
              ++$_beautifier_indent;
            }
            $result = rtrim($result) . $text;
            beautifier_br($result);
          }
          break;

        case '(':
          $result .= $text;
          ++$parenthesis;
          // Not multiline until proven so by whitespace.
          $in_multiline[$parenthesis] = FALSE;
          // If the $in_array flag for this parenthesis level was not
          // set previously, set it to FALSE.
          if (!isset($in_array[$parenthesis])) {
            $in_array[$parenthesis] = FALSE;
          }
          // Terminate function declaration, as a parenthesis indicates
          // the beginning of the arguments. This will catch all other
          // instances of parentheses, but in this case it's not a problem.
          $in_function_declaration = FALSE;
          break;

        case ')':
          if ($in_array[$parenthesis] && $in_multiline[$parenthesis]) {
            // Check if a comma insertion is necessary:
            $c = $position_last_significant_token;
            if ($result[$c] !== ',') {
              // We need to add a comma at $c:
              $result = substr($result, 0, $c + 1) .','. substr($result, $c + 1);
            }
          }
          if (!$in_quote && !$in_heredoc && (substr(rtrim($result), -1) == ',' || $in_multiline[$parenthesis])) {
            // Fix indent of right parenthesis in multiline structures by
            // increasing indent for each parenthesis and decreasing one level.
            $result = rtrim($result);
            beautifier_br($result, $parenthesis - 1);
            $result .= $text;
          }
          else {
            $result .= $text;
          }
          if ($parenthesis) {
            // Current parenthesis level is not an array anymore.
            $in_array[$parenthesis] = FALSE;
            --$parenthesis;
          }
          break;

        case '@':
          $in_at = true;
          $result .= $text;
          break;

        case ',':
          $result .= $text .' ';
          break;

        case '.':
          if (substr(rtrim($result), -1) == "'" || substr(rtrim($result), -1) == '"') {
            // Write string concatenation character directly after strings.
            $result = rtrim($result) . $text .' ';
          }
          else {
            $result = rtrim($result) .' '. $text .' ';
          }
          break;

        case '=':
        case '<':
        case '>':
        case '+':
        case '*':
        case '/':
        case '|':
        case '^':
        case '%':
          $result = rtrim($result) .' '. $text .' ';
          break;

        case '&':
          if (substr(rtrim($result), -1) == '=' || substr(rtrim($result), -1) == '(' || substr(rtrim($result), -1) == ',') {
            $result .= $text;
          }
          else {
            $result = rtrim($result) .' '. $text;
            // Ampersands used to declare reference return value for
            // functions should not have trailing space.
            if (!$in_function_declaration) {
              $result .= ' ';
            }
          }
          break;

        case '-':
          $result = rtrim($result);
          // Do not add a space before negative numbers or variables.
          $c = substr($result, -1);
          // Do not add a space between closing parenthesis and negative arithmetic operators.
          if ($c == '(') {
            $result .= ltrim($text);
          }
          // Add a space in front of the following chars, but not after them.
          elseif ($c == '>' || $c == '=' || $c == ',' || $c == ':' || $c == '?') {
            $result .= ' '. $text;
          }
          // Default arithmetic operator behavior.
          else {
            $result .= ' '. $text .' ';
          }
          break;

        case '"':
          // Toggle quote if the char is not escaped.
          if (rtrim($result) != "\\") {
            $in_quote = $in_quote ? false : true;
          }
          if (substr($result, -3) == ' . ') {
            // Write string concatenation character directly before strings.
            $result = rtrim($result);
          }
          $result .= $text;
          break;

        default:
          $result .= $text;
          break;
      }

      // All text possibilities are significant:
      $position_last_significant_token = strlen(rtrim($result)) - 1;

      // Because they are all significant, we cannot possibly be after
      // a comment now.
      $after_comment = FALSE;
      $after_initial_comment = FALSE;

      // TODO: Make resetting context flags easier to do.
    }
    else {
      // If we get here, then we have found not a single char, but a token.
      // See <http://www.php.net/manual/en/tokens.php> for a reference.

      // Fetch token array.
      list($id, $text) = $token;

      switch ($id) {
        case T_ARRAY:
          // Write array in lowercase.
          $result .= strtolower(trim($text));
          // Mark the next parenthesis level (we haven't consumed that token
          // yet) as an array.
          $in_array[$parenthesis + 1] = TRUE;
          break;

        case T_OPEN_TAG:
        case T_OPEN_TAG_WITH_ECHO:
          $in_php = true;
          // Add a line break between two PHP tags.
          if (substr(rtrim($result), -2) == '?>' && !$after_php) {
            beautifier_br($result);
          }
          $after_php = true;
          $nl = substr_count($text, "\n");
          $result .= trim($text);
          if ($first_php_tag) {
            beautifier_br($result);
            $first_php_tag = FALSE;
          }
          else {
            if ($nl) {
              beautifier_br($result, $parenthesis);
            }
            else {
              $result .= ' ';
            }
          }
          break;

        case T_CLOSE_TAG:
          $in_php = false;
          if ($after_php) {
            $result = rtrim($result, ' ') .' ';
            $text = ltrim($text, ' ');
          }
          // Do not alter a closing PHP tag ($text includes trailing white-space)
          // at all. Should allow to apply beautifier_format on phptemplate files.
          $result .= $text;
          break;

        case T_OBJECT_OPERATOR:
          $in_object = true;
          $result .= trim($text);
          break;

        case T_CONSTANT_ENCAPSED_STRING:
          // Handle special string concatenation case: 'bar' . 'baz'
          $c = substr($result, -3);
          if ($c == '". ' || $c == '\'. ') {
            $result = rtrim($result, ' .');
            $result .= ' . ';
          }
          // Handle special string concatenation case: 'bar'. $foo
          elseif (substr($result, -2) == '. ') {
            $result = rtrim($result);
          }
          // Move on to T_STRING / T_VARIABLE.
        case T_STRING:
        case T_VARIABLE:
          // No space after object operator ($foo->bar) and error suppression (@function()).
          if ($in_object || $in_at) {
            $result    = rtrim($result) . trim($text);
            $in_object = false;
            $in_at     = false;
          }
          else {
            // Insert a space after right parenthesis, but not after type casts.
            if (!in_array($lasttoken[0], array(T_ARRAY_CAST, T_BOOL_CAST, T_DOUBLE_CAST, T_INT_CAST, T_OBJECT_CAST, T_STRING_CAST, T_UNSET_CAST))) {
              beautifier_add_space($result);
            }
            $result .= trim($text);
          }
          $in_variable = true;
          break;

        case T_ENCAPSED_AND_WHITESPACE:
          $result .= $text;
          break;

        case T_WHITESPACE:
          // Avoid duplicate line feeds outside arrays.
          $c = ($parenthesis || $after_comment) ? 0 : 1;

          for ($c, $cc = substr_count($text, "\n"); $c < $cc; ++$c) {
            // Newlines were added; not after semicolon anymore
            beautifier_br($result, $parenthesis);
          }

          // If there were newlines present inside a parenthesis,
          // turn on multiline mode.
          if ($cc && $parenthesis) {
            $in_multiline[$parenthesis] = TRUE;
          }

          // If there were newlines present, move inline comments above.
          if ($cc) {
            $after_semicolon = FALSE;
            $after_case      = FALSE;
            $after_php       = FALSE;
          }

          $in_variable = FALSE;
          break;

        case T_SWITCH:
          ++$switches;
          // Purposely fall through.
        case T_IF:
        case T_FOR:
        case T_FOREACH:
        case T_GLOBAL:
        case T_STATIC:
        case T_ECHO:
        case T_PRINT:
        case T_NEW:
        case T_REQUIRE:
        case T_REQUIRE_ONCE:
        case T_INCLUDE:
        case T_INCLUDE_ONCE:
        case T_VAR:
          beautifier_add_space($result);
          // Append a space.
          $result .= trim($text) .' ';
          break;

        case T_DO:
          $result .= trim($text);
          $in_do_while = true;
          break;

        case T_WHILE:
          if ($in_do_while && substr(rtrim($result), -1) === '}') {
            // Write while after right parenthesis for do {...} while().
            $result = rtrim($result) .' ';
            $in_do_while = false;
          }
          // Append a space.
          $result .= trim($text) .' ';
          break;

        case T_ELSE:
        case T_ELSEIF:
          // Write else and else if to a new line.
          $result = rtrim($result);
          beautifier_br($result);
          $result .= trim($text) .' ';
          break;

        case T_CASE:
        case T_DEFAULT:
          $braces_in_case[$switches] = 0;
          $result         = rtrim($result);
          $after_case     = true;
          if (!$in_case) {
            $in_case = true;
            // Add a line break between cases.
            if (substr($result, -1) != '{') {
              beautifier_br($result);
            }
          }
          else {
            // Decrease current indent to align multiple cases.
            --$_beautifier_indent;
          }
          beautifier_br($result);
          $result .= trim($text) .' ';
          break;

        case T_BREAK:
          // Write break to a new line.
          $result = rtrim($result);
          beautifier_br($result);
          $result .= trim($text);
          if ($in_case && !$braces_in_case[$switches]) {
            --$_beautifier_indent;
            $in_case = FALSE;
          }
          break;

        case T_RETURN:
          if ($in_case && !$braces_in_case[$switches]) {
            // Defer reduction of indent for later.
            ++$_beautifier_indent;
            $after_return_in_case = true;
          }
        case T_CONTINUE:
          beautifier_add_space($result);
          $result .= trim($text) .' ';
          // Decrease indent only if we're not in a control structure inside a case.
          if ($in_case && !$braces_in_case[$switches]) {
            --$_beautifier_indent;
            $in_case = false;
          }
          break;

        case T_ABSTRACT:
        case T_PRIVATE:
        case T_PUBLIC:
        case T_PROTECTED:
          // Class member function properties must be treated similar to
          // T_FUNCTION, but without line-break after the token. Because more
          // than one of these tokens can appear in front of a function token,
          // we need another white-space control variable.
          $result .= trim($text) .' ';
          $after_visibility_modifier = TRUE;
          break;

        case T_FUNCTION:
          $in_function_declaration = TRUE;
          // Fall through.
        case T_CLASS:
          // Write function and class to new lines.
          $result = rtrim($result);
          if (substr($result, -1) == '}') {
            beautifier_br($result);
          }
          if (!$after_visibility_modifier) {
            beautifier_br($result);
          }
          else {
            // This code only applies to T_FUNCTION; do not add a newline
            // after public/protected/private/abstract.
            $after_visibility_modifier = FALSE;
            $result .= ' ';
          }
          $result .= trim($text) .' ';
          break;

        case T_EXTENDS:
        case T_INSTANCEOF:
          // Add space before and after 'extends' and 'instanceof'.
          $result = rtrim($result);
          $result .= ' '. trim($text) .' ';
          break;

        case T_AND_EQUAL:
        case T_AS:
        case T_BOOLEAN_AND:
        case T_BOOLEAN_OR:
        case T_CONCAT_EQUAL:
        case T_DIV_EQUAL:
        case T_DOUBLE_ARROW:
        case T_IS_EQUAL:
        case T_IS_NOT_EQUAL:
        case T_IS_IDENTICAL:
        case T_IS_NOT_IDENTICAL:
        case T_IS_GREATER_OR_EQUAL:
        case T_IS_SMALLER_OR_EQUAL:
        case T_LOGICAL_AND:
        case T_LOGICAL_OR:
        case T_LOGICAL_XOR:
        case T_MINUS_EQUAL:
        case T_MOD_EQUAL:
        case T_MUL_EQUAL:
        case T_OR_EQUAL:
        case T_PLUS_EQUAL:
        case T_SL:
        case T_SL_EQUAL:
        case T_SR:
        case T_SR_EQUAL:
        case T_XOR_EQUAL:
          // Surround operators with spaces.
          if (substr($result, -1) != ' ') {
            // $result must not be trimmed to allow multi-line if-clauses.
            $result .= ' ';
          }
          $result .= trim($text) .' ';
          break;

        case T_COMMENT:
        case T_ML_COMMENT:
        case T_DOC_COMMENT:
          if (substr($text, 0, 3) == '/**') {
            // Prepend a new line.
            $result = rtrim($result);
            if (!$after_initial_comment) {
              beautifier_br($result);
            }
            else {
              // This probably will get set below, but it's good to
              // explicitly turn it off after the initial comment has
              // influenced behavior and now is not necessary.
              $after_initial_comment = FALSE;
            }
            beautifier_br($result);

            // Remove carriage returns.
            $text = str_replace("\r", '', $text);

            $lines = explode("\n", $text);
            $params_fixed = false;
            for ($l = 0; $l < count($lines); ++$l) {
              $lines[$l] = trim($lines[$l]);

              // Add a new line between function description and first parameter description.
              if (!$params_fixed && substr($lines[$l], 0, 8) == '* @param' && $lines[$l - 1] != '*') {
                $result .= ' *';
                beautifier_br($result);
                $params_fixed = true;
              }
              else if (!$params_fixed && substr($lines[$l], 0, 8) == '* @param') {
                // Do nothing if parameter description is properly formatted.
                $params_fixed = true;
              }

              // Add a new line between function params and return.
              if (substr($lines[$l], 0, 9) == '* @return' && $lines[$l - 1] != '*') {
                $result .= ' *';
                beautifier_br($result);
              }

              // Add one space indent to get ' *[...]'.
              if ($l > 0) {
                $result .= ' ';
              }
              $result .= $lines[$l];
              if ($l < count($lines)) {
                beautifier_br($result);
              }
            }
          }
          else {
            // Move the comment above if it's embedded.
            $statement = false;
            // Some PHP versions throw a warning about wrong parameter count for
            // substr_count().
            $cc = substr_count(substr($result, $position_last_significant_token), "\n");
            if ((!$cc || $after_semicolon) && !$after_case) {
              $nl_position     = strrpos(rtrim($result, " \n"), "\n");
              $statement       = substr($result, $nl_position);
              $result          = substr($result, 0, $nl_position);
              $after_semicolon = false;
              beautifier_br($result, $parenthesis);
            }
            $result .= trim($text);
            beautifier_br($result, $parenthesis);
            if ($statement) {
              // Newlines are automatically added, so remove these.
              $result = rtrim($result, "\n ");
              $result .= rtrim($statement, "\n ");
              beautifier_br($result, $parenthesis);
              // Need to update this, as our comment trickery has just
              // reshuffled the index.
              $position_last_significant_token = strlen(rtrim($result, " \n")) - 1;
            }
            else {
              if (strpos($text, '$' . 'Id$') === FALSE) {
                $after_comment = TRUE;
              }
              else {
                // Is the number two so that our bottom code doesn't override
                // our flag immediately.
                $after_initial_comment = 2;
              }
            }
          }
          break;

        case T_INLINE_HTML:
          $result .= $text;
          break;

        case T_START_HEREDOC:
          $result .= trim($text);
          beautifier_br($result, FALSE, FALSE);
          $in_heredoc = TRUE;
          break;

        case T_END_HEREDOC:
          $result .= trim($text);
          beautifier_br($result, FALSE, FALSE);
          $in_heredoc = FALSE;
          break;

        default:
          $result .= trim($text);
          break;
      }

      // Store last token.
      $lasttoken = $token;

      // Excluding comments and whitespace, set the position of the
      // last significant token's last character to the length of the
      // string minus one.
      switch ($id) {
        case T_WHITESPACE:
        case T_COMMENT:
        case T_ML_COMMENT:
        case T_DOC_COMMENT:
          break;

        default:
          $position_last_significant_token = strlen(rtrim($result, " \n")) - 1;
          break;
      }

      if ($id !== T_COMMENT && $id !== T_ML_COMMENT) {
        $after_comment = FALSE;
      }
      if ($after_initial_comment && $id !== T_WHITESPACE) $after_initial_comment--;
    }
  }
  return $result;
}

function beautifier_br(&$result, $parenthesis = false, $add_indent = true) {
  global $_beautifier_indent;

  // Scan result backwards for whitespace.
  for ($i = strlen($result) - 1; $i >= 0; $i--) {
    if ($result[$i] == ' ') {
      continue;
    }
    if ($result[$i] == "\n") {
      $result = rtrim($result, ' ');
      break;
    }
    // Non-whitespace was encountered, no changes necessary.
    break;
  }

  if ($parenthesis) {
    // Add extra indent for each parenthesis in multiline definitions (f.e. arrays).
    $_beautifier_indent = $_beautifier_indent + $parenthesis;
    $result = rtrim($result);
    // This recursive call will only be done once, as $parenthesis is
    // set to false.
    beautifier_br($result, false, $add_indent);
    $_beautifier_indent = $_beautifier_indent - $parenthesis;
  }
  else {
    $output = "\n";
    if ($add_indent && $_beautifier_indent >= 0) {
      $output .= str_repeat('  ', $_beautifier_indent);
    }
    $result .= $output;
  }
}

function beautifier_add_space(&$result) {
  if (substr($result, -1) == ')') {
    $result .= ' ';
  }
}

function beautifier_trim($code = '') {
  // Remove surrounding whitespace.
  $code = trim($code);
  return $code;
}

function beautifier_insert_cvs_id($code = '') {
  // Insert CVS keyword Id.
  // Search in the very first 1000 chars, insert only one instance.
  if (strpos(substr($code, 0, 1000), '$Id') === false) {
    $code = preg_replace('/<\?php\n/', "<?php\n// \$Id\$\n\n", $code, 1);
  }
  return $code;
}

function beautifier_remove_closing_php($code = '') {
  // Remove closing PHP tag.
  if (substr($code, -2) == '?>') {
    $code = rtrim($code, '?>');
  }
  return $code;
}

function beautifier_append_two_lines($code = '') {
  // Append two empty lines.
  $code .= str_repeat(chr(10), 2);
  return $code;
}

function beautifier_line_breaks_win($code = '') {
  $task = array(
    '#search' => "@\r\n@",
    '#replace' => "\n",
  );
  return beautifier_replace($code, $task);
}

function beautifier_line_breaks_mac($code = '') {
  $task = array(
    '#search' => "@\r@",
    '#replace' => "\n",
  );
  return beautifier_replace($code, $task);
}

function beautifier_opening_php_full($code = '') {
  $task = array(
    '#search' => '@<\?(\s)@',
    '#replace' => "<?php$1",
  );
  return beautifier_replace($code, $task);
}

function beautifier_switch_exit($code = '') {
  $task = array(
    '#search' => '@
      (return   # match a return
        \s+     # - followed by some white-space
        .+      # - followed by any characters
        ;       # - followed by a semicolon
      )
      \s+       # match white-space (required)
      break;    # match a directly following "break;"
      @mx',
    '#replace' => '$1',
  );
  return beautifier_replace($code, $task);
}

function beautifier_inline_comment($code = '') {
  $task = array(
    '#search' => '@
      ^([\040\t]*)  # match spaces or tabs only.
      (?!case)      # do not match case statements.
      (\S.+?        # do not match lines containing only a comment.
        [;,{]       # match the TRICKY lines only.
      )
      [\040\t]*     # match spaces or tabs only.
      (?!:)         # do not match URL protocols.
      //\s*         # match inline comment token.
      ([^;\$]+?)$   # fetch comment, but do not match CVS keyword Id, nested comments, and comment tokens in quotes (f.e. "W3C//DTD").
      @mx',
    '#replace' => "$1// $3\n$1$2",
  );
  return beautifier_replace($code, $task);
}

function beautifier_cvs_id($code = '') {
  $task = array(
    '#search' => '@
      ^(          # match start of a line
        //.*      # match an inline comment followed by any characters
        \$Id.*\$  # match a CVS Id tag
      )$          # match end of a line
      @mx',
    '#replace' => "$1\n",
  );
  return beautifier_replace($code, $task);
}

function beautifier_multiple_vars($code = '') {
  $task = array(
    '#search' => '@
      ^(          # match start of a line
        \n?\ *    # match white-space, but only one new line
        \$.+?     # match a variable name
        \ =\      # match a variable assignment
        .+?$      # match a variable value
      ){3,}       # require the pattern to match at least 3 times
      @mx',
    '#replace_callback' => 'beautifier_replace_multiple_vars',
  );
  return beautifier_replace($code, $task);
}

function beautifier_replace_multiple_vars($matches) {
  // Retrieve all variable name = variable value pairs.
  $regex = '@
    ^           # match start of a line
    (\s*)       # match a single optional white-space char
    (\$.+?)     # match a variable name
    \ (.?)=\    # match a variable assignment
    (.+?$)      # match a variable value including end of line
    @mx';
  preg_match_all($regex, $matches[0], $vars, PREG_SET_ORDER);

  // Determine the longest variable name.
  $maxlength = 0;
  foreach ($vars as $var) {
    if (strlen($var[2]) > $maxlength) {
      $maxlength = strlen($var[2] . $var[3]);
    }
  }

  // Realign variable values at the longest variable names.
  $return = '';
  $extra_spaces = 0;
  for ($c = 0, $cc = count($vars); $c < $cc; ++$c) {
    if ($maxlength <= 20) {
      $extra_spaces = $maxlength - strlen($vars[$c][2] . $vars[$c][3]);
    }
    $return .= $vars[$c][1] . $vars[$c][2];
    $return .= str_repeat(' ', $extra_spaces) .' '. $vars[$c][3] .'= ';
    $return .= $vars[$c][4];
    if ($c < $cc - 1) {
      // Append a line break, but not to the last variable assignment.
      $return .= "\n";
    }
  }

  return $return;
}

function beautifier_curly_braces($code = '') {
  // This post-processor relies on the fact that beautifier_format already
  // re-formatted if statements without curly braces to be on one line.
  $task = array(
    '#search' => '@
      (\s*)           # match leading white-space, including newline
      (if\ \(.+\)\ )  # match if statement
      ([^\{].+;)      # match conditional executed code not starting with a curly brace, delimited by a semicolon.
      @x',
    '#replace' => '$1$2{$1  $3$1}',
  );
  return beautifier_replace($code, $task);
}

