
CONTENTS OF THIS FILE
----------------------

  * Introduction
  * Installation
  * Configuration


INTRODUCTION
------------
Maintainer: Daniel Braksator (http://drupal.org/user/134005)

Project page: http://drupal.org/project/beautifier.


INSTALLATION
------------
1. Copy beautifier folder to modules directory (usually sites/all/modules).
2. At admin/build/modules enable the Beautifier module.


CONFIGURATION
-------------
1. Enable permissions at admin/user/permissions.
2. Create beautifier block, or click 'Beautifier' in the Navigation menu, or 
   navigate to path: 'beautifier', e.g: yoursite.com/beautifier